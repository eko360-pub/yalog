package org.andriesfc.yalogdemo.java;

import android.app.Application;

import org.andriesfc.yalog.Log;
import org.andriesfc.yalog.config.BuiltInTagRewriteOperation.SdkSafe;
import org.andriesfc.yalog.config.ConfigBlock;
import org.andriesfc.yalog.config.Configuration;
import org.andriesfc.yalog.config.TagRewriteOperation;
import org.jetbrains.annotations.NotNull;


public final class YALogApp extends Application implements TagRewriteOperation {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.configure(this, new ConfigBlock() {
            @Override public void configure(Configuration builder) {
                builder.rewriteTagsWith(SdkSafe.INSTANCE);
                builder.rewriteTagsWith(YALogApp.this);
            }
        });
    }

    @NotNull
    @Override
    public String rewriteTag(@NotNull String tag) {
        return "YLD/" + tag;
    }
}
