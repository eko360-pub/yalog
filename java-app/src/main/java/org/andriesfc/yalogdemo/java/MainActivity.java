package org.andriesfc.yalogdemo.java;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.andriesfc.yalog.Log;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.Callable;


public final class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "Main ready! : {}", new Date());

        findViewById(R.id.btnLogFormatted).setOnClickListener(clickedBtnLogFormatted());
        findViewById(R.id.btnLogWithEx).setOnClickListener(clickedBtnLogWithException());
    }

    private View.OnClickListener clickedBtnLogFormatted() {
        return new View.OnClickListener() {
            Random rnd = new Random();
            @Override public void onClick(View v) {
                final Callable<Integer> a = nextInt();
                final Callable<Integer> b = nextInt();
                Callable result = new Callable() {
                    @Override public Object call() throws Exception {
                        return a.call() + b.call();
                    }
                };
                Log.d(TAG, "The result of {} + {} == {}", a, b, result);
            }

            Callable<Integer> nextInt() {
                return new Callable<Integer>() {
                    Integer v = null;
                    @Override public Integer call() {
                        if (v == null) {
                            Integer n = rnd.nextInt(10);
                            v = n;
                            return n;
                        } else  {
                            return v;
                        }
                    }
                };
            }

        };
    }

    private View.OnClickListener clickedBtnLogWithException() {
        return new View.OnClickListener() {
            @Override public void onClick(View v) {
                try {
                    throw new RuntimeException("So not want one wants.");
                } catch (Exception e) {
                    Log.e(TAG, e, "Expected!");
                }
            }
        };
    }


    private static final String TAG = MainActivity.class.getSimpleName();
}
