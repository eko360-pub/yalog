# YALOG - Yet another Android Logger

[![Download](https://api.bintray.com/packages/eko360/pub/yalog/images/download.svg)](https://bintray.com/eko360/pub/yalog/_latestVersion)

A drop in (sort off), replacement of Android's logging façade which allows for application wide configuration of logging concerns.

These concerns include out  of the box the following:  

* A more robust messaging template which will not break due to Java's formatting specification. 
* Ability to format tags as they are written out the underlying Android Log. 
* And lastly, only enable debugging of the underlying Android context if the debug flag has been set.

## Installation

Library support easy, in-code configuration, for both Java and Kotlin.

To use the library add the following depedendencies to your app(lication)s gradle file:

```groovy
repositories { 
	maven { url  "https://dl.bintray.com/eko360/pub" }
}

dependencies {
       implementation 'org.andriesfc.yalog:yalog:<version>'
}
```

## Configuration

### Basic ➙ Out of the box

The base configuration provides you with the following:

1. A message composer which uses simple place holders for message arguments, ex. `Log.d(TAG,"Current date is {}", Date())`
2. Only enables debug logging if the underlying context has a debug flag.
3. Following message formatter functions are applied (at the lowest possible priority):
   1. `java.util.Date` to ISO6801 time stamp format.
   2. `java.lang.Callable` are evaluated lazy and and the result is substituted into the message.
   3. Any kotlin function which conforms to `()->Any?` will be evaluated lazy and substituted into the message.

----

***Important***:  

1. In the absence of any configuration the following apply:
   - Debugging information is disabled, only message with a higher prorioty will be passed through.
   - Standard output parameters formatter are enabled.
2. Logging messages are hand-off direct to the appender, that is the on same thread as the calling thread. 
3. `Level.VERBOSE` is not enabled by default.

----

#### Kotlin Example

Activate default logging for Kotlin:

```kotlin
import android.app.Application
import org.andriesfc.yalog.*

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        configureLogging()
        Log.d(TAG, "Ready!")
    }

    companion object {
        private val TAG by simpleClassNameAsTag<YALogApp>()
    }
}
```

#### Java Example

```java
import static org.andriesfc.yalog.Configuration.configureLogging;
import static org.andriesfc.yalog.Tagging.simpleClassNameAsTag;

public final class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        configureLogging(this);
        Log.d(TAG, "App ready now : {}", new Date());
    }
    
    private static final Tag TAG = simpleClassNameAsTag(MyApp.class);
}
```

### Additional/Advance Configuration

The logging façade can re- configured with more advance options. For example: 

* Tag values can be modified before it gets written out to the appender log. A typical use case would be to prefix tags with something.
* Logs messages can be appended to multiple destinations.
* Log message hand off can be done asynchronously. 
* You can replace the message composer if you do not like to the default message compose format.

#### Kotlin Example

```kotlin
import android.app.Application
import org.andriesfc.yalog.*

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        configureLogging {
            setTagPrintable { tag -> "MyApp/$tag" }
            dispatchStrategy = SupportedDispatchStrategy.Async()
            addAppender(RemoteLogAppender())
            addAppender(TcpDebugAppender(port=2080))
        }
        Log.d(TAG, "Ready!")
    }

    companion object {
        private val TAG by simpleClassNameAsTag<MyApp>()
    }
}
```

#### Java Example

The same example in _Java_:

```java

import static org.andriesfc.yalog.Configuration.configureLogging;
import static org.andriesfc.yalog.Tagging.simpleClassNameAsTag;

public final class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        configureLogging(this);
        configureLogging(this, new Log.ConfigBlock() {
            @Override
            public void configureLogging(@NotNull LogConfig config) {
                config.setDispatchStrategy(new SupportedDispatchStrategy.Async());
                config.setTagPrintable(globalTagFormat());
                config.addAppender(new RemoteLogAppender());
                config.addAppender(new TcpDebugAppender(DEBUG_PORT));
            }
        });

        Log.d(TAG, "App ready now : {}", new Date());
    }


    private static final Tag TAG = simpleClassNameAsTag(MyApp.class);
    private static final int DEBUG_PORT = 2080;

    private static TagPrintable globalTagFormat() {
        return new TagPrintable() {
            @NotNull
            @Override
            public String printableTagOf(@NotNull Tag tag) {
                return "MyApp/" + tag.getCategory();
            }
        };
    }
}
```

 

