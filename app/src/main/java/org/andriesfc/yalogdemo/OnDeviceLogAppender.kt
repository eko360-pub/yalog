package org.andriesfc.yalogdemo

import android.content.Context
import android.util.JsonWriter
import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.internal.now
import org.andriesfc.yalog.message.LogMessage
import java.io.PrintWriter
import java.io.StringWriter
import java.io.Writer

/**
 * Logs to a an file on the device.
 *
 * Each line is complete JSON object in the following printableTagOf:
 *
 * ```json
 * {
 *   "ts": "<iso-date-time>",
 *   "declaringTag" :"<declaringTag>",
 *   "level": "<debug|warn|error|verbose>",
 *   "message": "<the-message>" ,
 *   "cause": { null |
 *   {
 *      "debugMessage":"<cause.message>,
 *      "trace":"<cause.stackTrace>"
 *   }
 * }
 * ```
 *
 * @param context The android context to access the device.
 * @param open A reference to function open a output stream.
 */
class OnDeviceLogAppender(
        private val writeToLog: ((Writer) -> Unit) -> Unit) : LogAppender {


    /**
     * Convenience constructor which opens a file named `app.log.json` (default) in append mode.
     *
     * @param context The android context.
     * @param fileName The name file which should be appended.
     *
     */
    constructor(context: Context, fileName:String = "app.log.json") : this({
        context.openFileOutput(fileName, Context.MODE_APPEND).bufferedWriter().use(it)
    })


    override fun append(message: LogMessage) = writeToLog {
        with(message) {
            JsonWriter(it).appendLog(tag, level, text, cause)
        }
    }

    companion object {

        private fun Writer.write(ch: Char) = write(ch.toInt())

        private inline val LogMessage.text: String get() = parts.joinToString(separator = "")

        private fun JsonWriter.appendLog(tag: String, level: Level, message: String, cause: Throwable?) {
            beginObject()
            name("ts").value(now())
            name("tag").value(tag)
            name("level").value(level.name)
            name("message").value(message)
            name("cause").value(cause)
            endObject()
        }

        private fun JsonWriter.value(cause: Throwable?) {
            when (cause) {
                null -> nullValue()
                else -> {

                    val stacktrace = StringWriter().run {
                        cause.printStackTrace(PrintWriter(this))
                        buffer.toString()
                    }

                    beginObject()
                    name("debugMessage").value(cause.message)
                    name("trace").value(stacktrace)
                    endObject()
                }
            }
        }
    }
}