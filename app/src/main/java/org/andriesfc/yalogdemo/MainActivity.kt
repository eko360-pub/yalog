package org.andriesfc.yalogdemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.andriesfc.yalog.Log
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG, "Hello from {}", this)

        btnLogWithEx.setOnClickListener {
            Log.e(TAG, RuntimeException("A real exception."), "Need some exception here.")
        }

        var x = 1
        btnLogFormatted.setOnClickListener {
            Log.d(TAG, "Formatted on {} [ID={}] {} {}", Date(), UUID.randomUUID(), { "[ Dynamically inserted here ]" }, { x++ })
        }
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }
}
