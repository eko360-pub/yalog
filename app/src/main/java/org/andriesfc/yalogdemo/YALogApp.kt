package org.andriesfc.yalogdemo

import android.app.Application
import org.andriesfc.yalog.Log
import org.andriesfc.yalog.config.BuiltInTagRewriteOperation.SdkSafeWithPrefix
import org.andriesfc.yalog.configureLogging

class YALogApp : Application() {

    override fun onCreate() {
        super.onCreate()
        configureLogging {
            rewriteTagsWith(SdkSafeWithPrefix("_YLA/"))
        }
        Log.d(TAG, "Ready!")
    }

    companion object {
        private const val TAG = "YALogApp"
    }
}
