# Module yalog

Yet another Android Logger


# Package org.andriesfc.yalog 

Base package, and home of the `Log` façade.


# Package org.andriesfc.yalog.appender

The basic android appender, and related interfaces for other appenders.


# Package org.andriesfc.yalog.config

Classes which handles the configuration of logging façade. 


# Package org.andriesfc.yalog.message

Classes which deal with messages, and message parts being appended. 


# Package org.andriesfc.yalog.processing

Classes which deal with the processing of log messages.


# Package org.andriesfc.yalog.utils

Some helper functions etc. Nothing here, please move on.
