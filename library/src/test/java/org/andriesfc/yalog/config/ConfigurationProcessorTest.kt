package org.andriesfc.yalog.config

import android.content.Context
import android.content.pm.ApplicationInfo
import io.mockk.*
import org.andriesfc.yalog.appender.AndroidLogAppender
import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.internal.isDebuggable
import org.andriesfc.yalog.message.DefaultMessageFactory
import org.andriesfc.yalog.message.MessageFactory
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*

@DisplayName("Configuration Processor Tests")
internal class ConfigurationProcessorTest {

    private lateinit var configuration: ConfigurationProcessor
    private lateinit var context: Context


    @BeforeEach
    fun setupProcessor() {

        context = spyk(recordPrivateCalls = true) {
            every { applicationInfo } returns ApplicationInfo().apply {
                flags = ApplicationInfo.FLAG_DEBUGGABLE
            }
        }

        configuration = spyk(ConfigurationProcessor(context), recordPrivateCalls = true)

        assertTrue(context.isDebuggable, "Expected context to be debuggable.")
    }

    @Test
    @DisplayName("Verify defaults.")
    fun testDebugDefaults() {

        val levelFilterSlot = slot<LevelFilter>()
        val defaultLogAppender = mutableListOf<LogAppender>()
        val messageFactorySlot = slot<MessageFactory>()

        every { configuration.filterOnLevel(capture(levelFilterSlot)) } just Runs
        every { configuration.addAppender(capture(defaultLogAppender)) } just Runs
        every { configuration.produceLogMessagesWith(capture(messageFactorySlot)) } just Runs

        configuration.defaults()

        verify { configuration.defaults() }

        assertTrue(levelFilterSlot.isCaptured)
        assertTrue(levelFilterSlot.captured.accept(Level.DEBUG))
        assertTrue(defaultLogAppender.size == 1)
        assertTrue(defaultLogAppender.first() is AndroidLogAppender)
    }

    @Test
    fun testNextProcessor() {
        val p = configuration.nextProcessor(1)
        println(p)
    }



}