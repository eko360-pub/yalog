package org.andriesfc.yalog.config

import org.andriesfc.yalog.config.BuiltInTagRewriteOperation.*
import org.andriesfc.yalog.config.BuiltInTagRewriteOperation.Companion.SDK_SAFE_TAG_LIMIT
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

@DisplayName("Test builtin tag rewrite functions.")
internal class BuiltInTagRewriteOperationTest {

    @Test
    @DisplayName("Tags should have no white spaces.")
    fun testTrimWhitespace() {
        val expectedTag = "tag"
        val tagWithWhitespace = "    $expectedTag  "
        val actualTag = TrimWhiteSpace.rewriteTag(tagWithWhitespace)
        assertEquals(expectedTag, actualTag)
    }

    @Test
    @DisplayName("Keeps tag as is.")
    fun testKeepAsIs() {
        val expectedTag = " tag "
        val actualTag = KeepAsIs.rewriteTag(expectedTag)
        assertEquals(expectedTag, actualTag)
    }

    @Test
    @DisplayName("Drops vowels from tag as an effort to shorten it.")
    fun dropVowelsToShorten() {
        val expectedTag = "CstmrFrm"
        val initialTag = "CustomerForm"
        val actualTag = DropVowelsToShorten.rewriteTag(initialTag)
        assertEquals(expectedTag, actualTag)
    }

    @Test
    @DisplayName("Only drop lower case vowels inorder to preserve camel case.")
    fun testDropOnlyLowercaseVowels() {
        val expected = "AndOk"
        val actual = DropVowelsToShorten.rewriteTag(expected)
        assertEquals(expected, actual)
    }

    @Test
    @DisplayName("Truncate start of tag of size exceeds SDK limit ($SDK_SAFE_TAG_LIMIT)")
    fun testForceDropFromStartIfExceedBuiltInTag() {

        val bigTag = "thisIsASomewhatBigTagApplesAreGreenSoIsJoeAndHisCarDriving"
        val expected = "SoIsJoeAndHisCarDriving"

        assertTrue(expected.length == SDK_SAFE_TAG_LIMIT, "Android tag limit changed! Please change the expectation.")

        val actual = ForceDropStartIfExceedsBuiltInLimit.rewriteTag(bigTag)

        assertEquals(expected, actual)
    }

    @DisplayName("Constant of tag limit for lowest common denominator should be 23.")
    fun testConstantOfTagLimit() {
        assertEquals(23, SDK_SAFE_TAG_LIMIT)
    }

    @Nested
    @DisplayName("Testing of the default \"AndroidSdKLimitShortened\" builtin strategy")
    inner class AndroidSdKLimitShortenedTests {

        @Test
        @DisplayName("Tag should only be modified if the size exceeds $SDK_SAFE_TAG_LIMIT characters.")
        fun testOnlyModifyTagIfItExceedsAndroidLimit() {
            val bigTag = "thisIsASomewhatBigTagApplesAreGreenSoIsJoeAndHisCarDriving"
            val actual = SdkSafe.rewriteTag(bigTag)
            assertTrue(actual.length < bigTag.length)
        }

        @ParameterizedTest(name = "{index} ✋ \"{0}\" should not have whitespace.")
        @DisplayName("Tags should never have white space")
        @CsvSource(
                "'thisIsASomewhatBigTagApplesAreGreenSoIsJoeAndHisCarDriving      '",
                "'  myTag  '")
        fun testShouldAlwaysRemoveWhitespace(testTag: String) {
            val actual = SdkSafe.rewriteTag(testTag)
            val whitespaceFound = actual.contains(" ")
            assertFalse(whitespaceFound, "Whitespace has not been removed from \"$actual\"")
        }

        @ParameterizedTest(name = "{index} ☞ {0} ➝ {1} ")
        @DisplayName("Vowels should only be dropped the size exceeds $SDK_SAFE_TAG_LIMIT")
        @CsvSource(delimiter = '|', value = [
            "World|World",
            "thisIsASomewhatBigTagApplesAreGreenSoIsJoeAndHisCarDriving|lsArGrnSIsJAndHsCrDrvng"
        ])
        fun shouldOnlyDropVowelsIfExceedsAndroidLimit(tag: String, expected: String) {
            val actual = SdkSafe.rewriteTag(tag)
            assertEquals(expected, actual)
        }
    }

}