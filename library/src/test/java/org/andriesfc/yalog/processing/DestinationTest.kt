package org.andriesfc.yalog.processing

import com.thedeanda.lorem.LoremIpsum
import io.mockk.*
import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.config.LevelFilter
import org.andriesfc.yalog.config.TagFilter
import org.andriesfc.yalog.config.TagRewriteOperation
import org.andriesfc.yalog.message.DefaultMessageFactory
import org.andriesfc.yalog.message.LogMessage
import org.andriesfc.yalog.message.TestMessageFactory.createMessage
import org.andriesfc.yalog.message.part.PartFactory
import org.andriesfc.yalog.message.render
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Log Appender destination selection and logic tests.")
internal class DestinationTest {

    private lateinit var tagRewriteOperation: TagRewriteOperation
    private lateinit var appender: LogAppender
    private lateinit var tagFilter: TagFilter
    private lateinit var levelFilter: LevelFilter
    private lateinit var destination: Destination

    private val messageFactory = DefaultMessageFactory(PartFactory())
    private val loremIpsum = LoremIpsum.getInstance()

    @BeforeEach
    fun setUp() {

        tagRewriteOperation = mockk(relaxed = true)
        appender = mockk(relaxed = true)
        tagFilter = mockk(relaxed = true)
        levelFilter = mockk(relaxed = true)

        destination = Destination(appender, tagFilter, levelFilter, null)
    }


    @Test
    @DisplayName("Destination should be able to select/not-select, appender for passing, and non passing.")
    fun eitherLevelOrTagFilterShouldReject() {

        val passingTag = "tag"
        every { tagFilter.accept(passingTag) }.returns(true)

        val passingLevel = Level.DEBUG
        every { levelFilter.accept(passingLevel) }.returns(true)

        val notPassingTag = "someNonPassingTag"

        assertNotNull(destination.select(passingLevel, passingTag),
                "Should have selected destination.")

        assertNull(destination.select(passingLevel, notPassingTag),
                "Should not have selected, for passingLevel and nonPassingLevel.")
    }

    @Test
    @DisplayName("When destination accepts a level and tag, it should select the configured appender.")
    fun acceptedLevelAndTagIsSelecting() {

        every { tagFilter.accept(any()) } returns true
        every { levelFilter.accept(any()) } returns true

        val receivingAppender = destination.select(Level.DEBUG, "tag")

        assertNotNull(receivingAppender, "Destination should have selected an appender.")


    }

    @Test
    @DisplayName("Destination should forward start/stop to appender if it is supported.")
    fun testTargetAppenderLifeCycle() {

        val appenderAsProcessor = mockk<LogAppender>(
                relaxed = true,
                moreInterfaces = *arrayOf(Processing::class))

        val destination = Destination(appenderAsProcessor, tagFilter, levelFilter, tagRewriteOperation)

        every { (appenderAsProcessor as Processing).start() } returns Unit
        every { (appenderAsProcessor as Processing).stop() } returns Unit

        destination.start()
        destination.stop()

        verifySequence {
            (appenderAsProcessor as Processing).start()
            (appenderAsProcessor as Processing).stop()
        }
    }

    @Test
    @DisplayName("ToString should produce atleast some debug info.")
    fun testToString() {
        val destination = Destination(appender, tagFilter, levelFilter, tagRewriteOperation).toString()
        assertTrue(destination.isNotEmpty() || destination.isNotBlank())
    }

    @Test
    @DisplayName("Destination should rewrite the tag if destination has been configured to so.")
    fun testSelectableShouldRewriteTag() {

        val prefix = "_Y/"
        val rewrite = { tag: String -> "$prefix$tag" }
        val tag = "CustomerView"
        val expectedTag = rewrite(tag)
        val actualMessage = slot<LogMessage>()
        val level = Level.DEBUG
        val message = createMessage(level, tag)

        every { tagRewriteOperation.rewriteTag(any()) } answers { rewrite(arg(0)) }
        every { appender.append(capture(actualMessage)) } just Runs
        every { tagFilter.accept(any()) } returns true
        every { levelFilter.accept(any()) } returns true

        Destination(
                appender,
                tagFilter,
                levelFilter,
                tagRewriteOperation)
                .select(level, tag)
                ?.append(message)
                ?: fail("Mocked appender has no been selected.")

        verifyAll {
            appender.append(any())
            tagRewriteOperation.rewriteTag(any())
            levelFilter.accept(any())
        }

        assertEquals(expectedTag, actualMessage.captured.tag)
    }


    companion object {

        private fun LogMessage.debug() = """
            LogMessage {
                id = "$id";
                timestamp = "$timestamp";
                tag = "$tag";
                level = "$level";
                content = "${parts.render()}"

            }
        """.trimIndent()

    }
}