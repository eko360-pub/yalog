package org.andriesfc.yalog.message

import com.thedeanda.lorem.LoremIpsum
import org.andriesfc.yalog.appender.Level

object TestMessageFactory : MessageFactory by DefaultMessageFactory() {

    private val loremIpsum = LoremIpsum.getInstance()

    fun createMessage(level: Level, tag: String, minWord: Int = 10, maxWords: Int = 20) : LogMessage {
        val content = loremIpsum.getWords(minWord, maxWords)
        return createMessage(level, tag, null, content, NO_ARGS)
    }

    fun createLargeMessage(level: Level, tag: String): LogMessage {
        val content = loremIpsum.getParagraphs(5, 8)
        return createMessage(level, tag, null, content, NO_ARGS)
    }

    private val NO_ARGS = emptyArray<String>()

}