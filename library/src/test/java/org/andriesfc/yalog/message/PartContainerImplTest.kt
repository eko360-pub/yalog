package org.andriesfc.yalog.message

import org.andriesfc.yalog.internal.now
import org.andriesfc.yalog.message.part.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.lang.Math.PI
import java.util.*


@DisplayName("Parts Container Tests")
internal class PartContainerImplTest {

    private val partFactory = PartFactory()
    private fun partsOf(template: String, vararg args: Any?) = partFactory.build(template, args.toMutableList())

    @Test
    @DisplayName("Container should contain same items as the container list.")
    fun createContainerFromListOfParts() {
        val partsList = partsOf("Hello {} {}. Time to {}", "sunny", "world", "have a moment.")
        val container = partsList.toContainer()
        assertEquals(partsList, container.toList())
    }

    @Nested
    @DisplayName("Accessing container with in the container.")
    inner class AccessTests {

        private val ano = 12
        private val template = "Hello {}. Time for {}. Done! N={}"
        private val args = arrayOf("Joe", "some fun", ano)
        private val parts by lazy { partsOf(template, args) }
        private val container by lazy { parts.toContainer() }
        private val expectedPartSize = 6

        @Test
        @DisplayName("Parts should be accessible by index.")
        fun shouldBeAbleToAccessByIndex() {
            assertEquals(expectedPartSize, parts.size)
            assertDoesNotThrow {
                for (i in 0 until parts.size) container[i]
            }
        }

        @Test
        @DisplayName("Accessing container by index should be in the exact order of the source list.")
        fun accessingByIndexPreserveOrder() {
            for (i in 0 until parts.size) {
                assertEquals(parts[i], container[i])
            }
        }

        @Test
        @DisplayName("There should be no null container in the container list.")
        fun containerShouldHaveNoNulls() {

            val template = "Jane is {}. So should we {}. {},{}"
            val args = listOf("here", "run", 10, 12.300289)
            val parts = partFactory.build(template, args)

            println("parts = $parts")

            val nullParts = parts.asSequence()
                    .filterIsInstance<Part.ValuePart<out Any?>>()
                    .filter { it.value == null }
                    .toList()

            val actualNullParts = nullParts.count()
            val expectedNullParts = 0

            println("Null Parts = $nullParts")

            assertEquals(expectedNullParts, actualNullParts) { "Values are null, but should not be: $nullParts" }

        }
    }

    @Nested
    @DisplayName("Modifying container of the container.")
    inner class ModifyingTests {

        @Test
        @DisplayName("Parts should be replaceable with other container.")
        fun partShouldBeReplaceable() {

            val parts = partsOf("Morning {}. The world is a {} place. Today's date is {}.", "Joe", "happy", now()).toContainer()

            val replacedGreeting = partFactory.partOf("Good afternoon ")

            parts[0] = replacedGreeting

            assertEquals(replacedGreeting, parts[0])

            println(parts.combine())

        }

        @Test
        @DisplayName("Modifiable part should be updatable.")
        fun modifiablePartShouldBeUpdatable() {

            val parts = partsOf("Morning {1}. The world is a {2} place.", "Joe", "happy").toContainer()

            val messageOut = parts.combine()
            println(messageOut)

            parts.select<String, Part.ModifiableValuePart<String>> { it.value == "happy" }.first().value = "sad"

            val expectedMessage = "Morning Joe. The world is a sad place."
            val actualMessage = parts.combine()

            assertEquals(expectedMessage, actualMessage)
        }
    }

    @Nested
    @DisplayName("Attaching meta data to container and/or the container.")
    inner class AttachmentTests {

        private fun buildContainer() = partsOf("Welcome {}.", "Joe").toContainer()

        @Test
        @DisplayName("Should be able to add attachment to a container.")
        fun testAddAttachmentToContainer() {

            val container = buildContainer()

            val expectedKey = "one"
            val expectedValue: Int? = 1
            container.attach(expectedKey, expectedValue)

            println(container.attachments)

            val actualValue = container.attachments[expectedKey]?.value

            assertEquals(expectedValue, actualValue)

        }

        @Test
        @DisplayName("Should be able to add an attachment to an attachment.")
        fun testShouldBeAbleToAttachUntoAnotherAttachment() {

            val container = buildContainer()
            val expectedChildren = 10

            val a = container.attach(System.currentTimeMillis()).apply {
                for (i in 1 .. expectedChildren) {
                    attach("child-$i", (i + System.currentTimeMillis()).toDouble() / PI )
                }
            }

            println(a)

            assertEquals(expectedChildren, a.attachments.size)
        }

        @Test
        @DisplayName("Should be able to remove attachments.")
        fun testAbleToRemove() {


            val attachedValue = 12
            val container = buildContainer()
            val attachment = container.attach(attachedValue)

            assertTrue(container.attachments.isNotEmpty())

            println(attachment)

            attachment.detach()

            assertTrue(container.attachments.isEmpty())
        }

        @Test
        @DisplayName("Should be able to access an attachment by key.")
        fun testAccessByKey() {

            val container = buildContainer()
            val key = "key"

            container.attach(key, Date())

            assertTrue(key in container.attachments)

        }

        @Test
        @DisplayName("Should be able to access an attachments by value type.")
        fun testAccessByValueType() {

            val container = buildContainer()

            val printIt = { it:Any? -> println(it) }

            val a1 = container.attach(12).also(printIt)
            val a2 = container.attach(16.20).also(printIt)
            val a3 = container.attach(true).also(printIt)

            assertEquals(3, container.attachments.size)

            assertEquals(a1, container.selectAttachments<Int>().first())
            assertEquals(a2, container.selectAttachments<Double>().first())
            assertEquals(a3, container.selectAttachments<Boolean>().first())
        }

        @Test
        @DisplayName("Should be able to access an attachment by key and type.")
        fun testAccessByKeyAndType() {

            val container = buildContainer().apply {
                for (i in 1 .. 10) {
                    val key  = "key-$i"
                    when {
                        i % 2 == 0 ->  attach(key, i / 2)
                        else -> attach(key, i.toDouble() / 3.15)
                    }
                }
            }

            println(container.attachments)


            val selection = container.selectAttachments<Double> { it.key.contains("-3") }.toList()

            assertEquals(1, selection.size)

            println("""
                SELECTION :
                $selection
            """.trimIndent())

            assertTrue(selection.isNotEmpty())

        }

    }
}
