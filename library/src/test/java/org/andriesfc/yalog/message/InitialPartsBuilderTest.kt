@file:Suppress("unused")

package org.andriesfc.yalog.message

import org.andriesfc.yalog.message.part.PartFactory
import org.andriesfc.yalog.message.part.build
import org.andriesfc.yalog.message.part.combine
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@DisplayName("Part Builder")
class InitialPartsBuilderTest {

    private val partFactory = PartFactory()

    data class PartAgreement(val template: String, val args: List<Any?>, val expectedParts: Int)
    data class JoinedToString(val agreement: PartAgreement, val expectedString: String)

    @Nested
    @DisplayName("Creation of container")
    inner class PartCreation() {

        @ParameterizedTest
        @MethodSource("partAgreementTestData")
        @DisplayName("Number of container produced should agree with the message template.")
        fun partsProducedShouldAgree(agreement: PartAgreement) {
            with(agreement) {
                val parts = partFactory.build(template, args)
                assertEquals(expectedParts, parts.size, "Expected to have $expectedParts parts.")
            }
        }

        fun partAgreementTestData() = listOf(
                PartAgreement(template = "Hello {}. Go now {}", args = listOf("joe", "please"), expectedParts = 4),
                PartAgreement(template = "Hello {}", args = listOf("joe"), expectedParts = 2),
                PartAgreement(template = "Hello", args = emptyList(), expectedParts = 1),
                PartAgreement(template = "", args = emptyList(), expectedParts = 0))


        @ParameterizedTest
        @DisplayName("Simple container when joined together should produce the correct text message")
        @MethodSource("joinedToStringData")
        fun simplePartsShouldJoinUpCorrectly(joinedToString: JoinedToString) {

            val actualString = joinedToString.agreement.run {
                partFactory.build(template, args).combine()
            }

            assertEquals(joinedToString.expectedString, actualString)

        }

        fun joinedToStringData() = listOf(
                JoinedToString(
                        agreement = PartAgreement("Welcome {}.", listOf("joe"), 3),
                        expectedString = "Welcome joe."))
    }


}