package org.andriesfc.yalog.appender

import org.andriesfc.yalog.appender.Level.*
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@DisplayName("Correct filtering of levels based priority.")
internal class LevelTest {

    private val allLevels = Level.values().toSet()

    data class Data(
            val filtered: Level,
            val levels: Set<Level>)


    @ParameterizedTest
    @MethodSource("truthTable")
    @DisplayName("Only the expected requested levels should be allowed")
    internal fun testPassOnlyAtCorrectLevel(data: Data) {
        with(data) {
            val expectedToPass = levels
            val actuallyPassed = allLevels.asSequence().filter { filtered shouldAtLeastBeAllowed it }.toSet()
            assertEquals(expectedToPass, actuallyPassed)
        }
    }

    @ParameterizedTest
    @MethodSource("truthTable")
    @DisplayName("Only the expected requested levels should be blocked.")
    internal fun testBlockOnlyTheExpectedLevel(data: Data) {
        with(data.inverse()) {
            val expectedToBeBlocked = levels
            val actuallyBlocked = allLevels.asSequence().filterNot { filtered shouldAtLeastBeAllowed it }.toSet()
            assertEquals(expectedToBeBlocked, actuallyBlocked)
        }
    }


    private fun Data.inverse() = copy(levels = allLevels - levels)

    /**
     * Builds up grid of which level should be allowed to based on the requested level.
     */
    private fun truthTable(): List<Data> = values().map { requestedLevel ->
        when (requestedLevel) {
            OFF -> Data(requestedLevel, emptySet())
            VERBOSE -> Data(requestedLevel, setOf(VERBOSE, DEBUG, INFO, WARN, ERROR))
            DEBUG -> Data(requestedLevel, setOf(DEBUG, INFO, WARN, ERROR))
            INFO -> Data(requestedLevel, setOf(INFO, WARN, ERROR))
            WARN -> Data(requestedLevel, setOf(WARN, ERROR))
            ERROR -> Data(requestedLevel, setOf(ERROR))
        }
    }

}