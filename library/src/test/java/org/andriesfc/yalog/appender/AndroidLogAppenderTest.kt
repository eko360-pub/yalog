package org.andriesfc.yalog.appender

import io.mockk.every
import io.mockk.spyk
import io.mockk.verify
import org.andriesfc.yalog.message.TestMessageFactory
import org.andriesfc.yalog.message.render
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Android Log Appender Tests.")
class AndroidLogAppenderTest {

    private lateinit var androidLogAppender: AndroidLogAppender
    private val maxAllowedLength = 100

    @BeforeAll
    fun setupShared() {
        androidLogAppender = spyk(AndroidLogAppender(
                maxAllowedMessageLength = maxAllowedLength,
                continueOnNextMessage = "↩").also { println("USING: $it") },
                recordPrivateCalls = true)
    }

    @Test
    @DisplayName("Logging a large text block should be logged as a sequence of smaller pieces.")
    fun testMessageNeverExceedsAllowedMax() {

        val parts = mutableListOf<String>()
        val largeMessage = TestMessageFactory.createLargeMessage(Level.DEBUG, "tag")
        val largeMessageContent = largeMessage.parts.render()

        every {
            androidLogAppender["writeOutAndroidLog"](
                    any<String>(),
                    any<Level>(),
                    any<Throwable>(),
                    capture(parts))
        } returns Unit

        androidLogAppender.append(largeMessage)

        verify { androidLogAppender.append(any()) }

        assertTrue(parts.isNotEmpty(), "Message should have been broken down: ${largeMessageContent.substring(10)}...")

        assertTrue(parts
                .first()
                .endsWith(androidLogAppender.continueOnNextMessage),
                "There should have been at least one message ending in \"${androidLogAppender.continueOnNextMessage}\".")

        assertFalse(parts.last().endsWith(androidLogAppender.continueOnNextMessage),
                "Last part of the debug message should not end with an continuation string.")


    }

}