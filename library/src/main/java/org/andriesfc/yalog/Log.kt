package org.andriesfc.yalog

import android.content.Context
import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.appender.Level.*
import org.andriesfc.yalog.config.ConfigBlock
import org.andriesfc.yalog.config.Configuration
import org.andriesfc.yalog.config.ConfigurationProcessor
import org.andriesfc.yalog.internal.compose
import org.andriesfc.yalog.processing.Processor
import java.util.concurrent.atomic.AtomicReference
import kotlin.LazyThreadSafetyMode.PUBLICATION
import android.util.Log as fallbackLog


/**
 * Log façade which acts mostly as drop in replacement for the [android.util.Log] class.
 *
 * # Introduction
 *
 * This façade follows the android `Log` usage patterns. It supports the following
 * levels:
 *
 *  - Debug ⤳ `Log.d(...)`
 *  - Info  ⤳ `Log.i(...)`)
 *  - Warning ⤳  `Log.w(...)`
 *  - Error ⤳ 'Log.e(...)`
 *
 *
 * The library prefers to have the declaringTag either followed by a message, or by an exception,
 * as in the case of failure. This is to ensure that an exception does not inadvertently gets
 * passed as part of the variable argument list, as well as clearly communicating that the
 * intent of the developer was to convey an failure (expected, or otherwise).
 */
@Suppress("MemberVisibilityCanBePrivate")
object Log {

    @JvmStatic fun d(tag: String, message: String, vararg args: Any?) = submit(DEBUG, tag, message, args)
    @JvmStatic fun d(tag: String, cause: Throwable, message: String, vararg args: Any?) = submit(DEBUG, tag, message, args, cause)

    @JvmStatic fun i(tag: String, message: String, vararg args: Any?) = submit(INFO, tag, message, args)
    @JvmStatic fun i(tag: String, cause: Throwable, message: String, vararg args: Any?) = submit(INFO, tag, message, args, cause)

    @JvmStatic fun w(tag: String, message: String, vararg args: Any?) = submit(WARN, tag, message, args)
    @JvmStatic fun w(tag: String, cause: Throwable, message: String, vararg args: Any?) = submit(WARN, tag, message, args, cause)

    @JvmStatic fun e(tag: String, message: String, vararg args: Any?) = submit(ERROR, tag, message, args)
    @JvmStatic fun e(tag: String, cause: Throwable, message: String, vararg args: Any?) = submit(ERROR, tag, message, args, cause)

    @JvmStatic fun v(tag: String, message: String, vararg args: Any?) = submit(VERBOSE, tag, message, args)
    @JvmStatic fun v(tag: String, cause: Throwable, message: String, vararg args: Any?) = submit(VERBOSE, tag, message, args, cause)

    private fun submit(level: Level, tag: String, message: String, args: Array<out Any?>, cause: Throwable? = null) {
        val submit = processor.get()
        when (submit) {
            null -> {
                fallbackLog.w(tag, OOPS_MESSAGE, IllegalStateException(NOW_TRACE_FOLLOWS))
                val fallbackMessage by lazy(PUBLICATION) { message.compose(args.toList()) }
                when (level) {
                    ERROR -> fallbackLog.e(tag, fallbackMessage, cause)
                    DEBUG -> fallbackLog.d(tag, fallbackMessage, cause)
                    INFO -> fallbackLog.d(tag, fallbackMessage, cause)
                    WARN -> fallbackLog.d(tag, fallbackMessage, cause)
                    VERBOSE -> fallbackLog.d(tag, fallbackMessage, cause)
                    OFF -> Unit
                }
            }
            else -> submit(tag, level, cause, message, args)
        }
    }

    private val processor = AtomicReference<Processor?>()

    @Synchronized
    internal fun configure(context: Context?, configuration:Configuration.()-> Unit) {
        val next = ConfigurationProcessor(context).apply(configuration).nextProcessor(configurationVersion + 1)
        val current = processor.get()
        current?.stop()
        next.start()
        processor.set(next)
    }

    @Suppress("unused")
    @JvmStatic fun configure(context: Context?, configBlock: ConfigBlock) {
        configure(context) { configBlock.configure(this) }
        d(TAG, "{}", this)
    }

    init {
        configure(null) {} // Use defaults
    }

    override fun toString(): String = "${processor.get()?.toString()}"

    val configurationVersion: Int = processor.get()?.version ?: 0
    private const val TAG = "YALog"
    private const val OOPS_MESSAGE = "Oops! Something went wrong. Logging processor has not been configured fully, falling back to android logging."
    private const val NOW_TRACE_FOLLOWS : String = "Stack trace follows."

}


fun Context.configureLogging(init: Configuration.() -> Unit) {
    Log.configure(this, init)
}

