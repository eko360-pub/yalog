package org.andriesfc.yalog.config

/**
 * Indicate that an supported component wants to contribute something to it's own
 * configuration.
 *
 * @param C Represents the contributor
 */
interface Contributor<out C>






