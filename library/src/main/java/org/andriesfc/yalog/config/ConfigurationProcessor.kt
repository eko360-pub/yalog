package org.andriesfc.yalog.config

import android.content.Context
import org.andriesfc.yalog.appender.AndroidLogAppender
import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.appender.TagRewriteContributor
import org.andriesfc.yalog.internal.addOnce
import org.andriesfc.yalog.internal.isDebuggable
import org.andriesfc.yalog.message.DefaultMessageFactory
import org.andriesfc.yalog.message.MessageFactory
import org.andriesfc.yalog.message.part.PartFactory
import org.andriesfc.yalog.processing.*

/**
 * Processes logging configuration for a given android context.
 */
class ConfigurationProcessor(private val context: Context?) : Configuration {

    @Suppress("SpellCheckingInspection")
    private val appenders = mutableListOf<LogAppender>()
    private val tagFilters = mutableListOf<TagFilter>()
    private val levelFilters = mutableListOf<LevelFilter>()
    private val messageFilters = mutableListOf<LogMessageFilter>()
    private var tagRewriteOperation: TagRewriteOperation? = null
    private lateinit var messageFactory: MessageFactory
    private var messageDistributor: Distributor? = null

    init {
        defaults()
    }

    override fun defaults() {

        clear()

        setLeastLevel(when {
            context == null -> Level.INFO
            context.isDebuggable -> Level.DEBUG
            else -> Level.INFO
        })

        addAppender(AndroidLogAppender())

    }

    override fun clear() {
        appenders.clear()
        tagFilters.clear()
        levelFilters.clear()
        messageFilters.clear()
        tagRewriteOperation = null
        messageFactory = DefaultMessageFactory()
    }

    override fun addAppender(appender: LogAppender) {
        appenders.addOnce(appender)
    }

    override fun addTagFilter(tagFilter: TagFilter) {
        tagFilters.addOnce(tagFilter)
    }

    override fun filterOnLevel(levelFilter: LevelFilter) {
        levelFilters.addOnce(levelFilter)
    }

    override fun addMessageFilter(messageFilter: LogMessageFilter) {
        messageFilters.addOnce(messageFilter)
    }

    override fun rewriteTagsWith(tagRewriteOperation: TagRewriteOperation) {
        this.tagRewriteOperation = tagRewriteOperation
    }

    override fun distributeMessagesWith(distributor: Distributor) {
        messageDistributor = null
    }

    override fun produceLogMessagesWith(messageFactory: MessageFactory) {
        this.messageFactory = messageFactory
    }

    fun nextProcessor(nextVersion:Int): Processor {

        val configuredDistributor = configureDistributor()
        val configuredTagFilters = tagFilters.rejectedByNone()
        val configuredLevelFilters = levelFilters.rejectedByNone()
        val configuredMessageFilter = messageFilters.rejectedByNone()

        fun rewriteHandlerOf(appender: LogAppender): TagRewriteOperation {
            return (listOf<TagRewriteOperation>()
                    + tagRewriteOperation
                    + (appender as? TagRewriteContributor)?.tagRewriteContribution)
                    .filterNotNull()
                    .chainHandlers()
        }

        val destinations = appenders.map { appender ->
            Destination(
                    target = appender,
                    tagFilter = configuredTagFilters,
                    levelFilter = configuredLevelFilters,
                    tagRewriteOperation = rewriteHandlerOf(appender))
        }

        return Processor(
                nextVersion,
                configuredDistributor,
                destinations,
                messageFactory,
                configuredMessageFilter
        )
    }

    private fun configureDistributor(): Distributor = messageDistributor ?: when {
        appenders.size == 1 -> DirectDistributor()
        appenders.size > 1 -> FireAndForgetDistributor()
        else -> DirectDistributor()
    }


}