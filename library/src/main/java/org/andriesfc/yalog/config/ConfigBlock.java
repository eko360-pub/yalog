package org.andriesfc.yalog.config;

/**
 * Just a convenience interface which allows the use anonymous inner class to access the
 * configuration. Most notably used for Java based configuration.
 */
public interface ConfigBlock {
    void configure(Configuration builder);
}
