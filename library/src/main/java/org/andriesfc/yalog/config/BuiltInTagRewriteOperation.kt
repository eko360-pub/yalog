package org.andriesfc.yalog.config

import org.andriesfc.yalog.internal.couldBeTrimmed

/**
 * Various built in tag rewrite operations.
 *
 * The following built-ins are provided:
 *
 * - `KeepAsIs` ➜ Keep the tag as is.
 * - `TrimWhiteSpace` ➜ Removes white space from around the tag
 * - `DropVowelsToShorten` ➜ Drop all vowels.
 * - `ForceDropFromStartIfExceedBuiltInTag` ➜ Drop first part of the tag if exceeds 23 characters
 * - `AndroidSdKLimitShortened` ➜ _**Combines**_ the above, but only apply if the length of the tag is more than 23 characters
 *
 */
sealed class BuiltInTagRewriteOperation(private val name: String) : TagRewriteOperation {

    override fun toString(): String = "BuiltInTagRewriteOperation.$name"

    /**
     * Removes white space from around the tag
     */
    object TrimWhiteSpace : BuiltInTagRewriteOperation(TrimWhiteSpace::class.java.simpleName) {
        override fun rewriteTag(tag: String): String {
            return tag.trim()
        }
    }

    /**
     * Keep the tag as is.
     */
    object KeepAsIs : BuiltInTagRewriteOperation(KeepAsIs::class.java.simpleName) {
        override fun rewriteTag(tag: String): String {
            return tag
        }
    }

    /**
     * Drop all vowels.
     */
    object DropVowelsToShorten : BuiltInTagRewriteOperation(DropVowelsToShorten::class.java.simpleName) {

        private val Char.isVowel: Boolean
            get() = this == 'a'
                    || this == 'e'
                    || this == 'i'
                    || this == 'o'
                    || this == 'u'

        override fun rewriteTag(tag: String): String = buildString {
            for (c in tag) {
                if (!c.isVowel) {
                    append(c)
                }
            }
        }
    }

    /**
     * Drop first part of the tag if exceeds 23 characters
     */
    object ForceDropStartIfExceedsBuiltInLimit : BuiltInTagRewriteOperation(ForceDropStartIfExceedsBuiltInLimit::class.java.simpleName) {
        override fun rewriteTag(tag: String): String {
            return if (tag.length < SDK_SAFE_TAG_LIMIT) tag else {
                val redundant = tag.length - SDK_SAFE_TAG_LIMIT
                tag.substring(redundant, tag.length)
            }
        }
    }

    /**
     * _**Combines**_ the above, but only apply if the length of the tag is more than 23 characters
     */
    object SdkSafe : BuiltInTagRewriteOperation(SdkSafe::class.java.simpleName) {

        private val trimSequence = run {

            fun trimIfExceeds(trim: (String) -> String): TagRewriteOperation = rewriteTag { tag ->
                if (tag.length > SDK_SAFE_TAG_LIMIT) trim(tag) else tag
            }

            arrayOf(
                    TrimWhiteSpace,
                    trimIfExceeds { tag -> DropVowelsToShorten.rewriteTag(tag) },
                    trimIfExceeds { tag -> ForceDropStartIfExceedsBuiltInLimit.rewriteTag(tag) })
        }

        override fun rewriteTag(tag: String): String {
            return trimSequence.fold(tag) { last, next -> next.rewriteTag(last) }
        }

    }

    /**
     * Provides a tag rewriting strategy which will preserve the a [prefix] at all cost, while
     * still keeping the overall tag less than the limits imposed by the legacy android SDKs.
     */
    class SdkSafeWithPrefix(
            private val prefix: String,
            hardSafeTagLimit: Int = SDK_SAFE_TAG_LIMIT
    ) : BuiltInTagRewriteOperation(SdkSafeWithPrefix::class.java.simpleName) {

        private val safeTagLimit: Int = hardSafeTagLimit - prefix.length

        init {

            if (prefix.couldBeTrimmed()) {
                throw IllegalArgumentException("""
                    Prefix value of \"$prefix\" should not have whitespace around it.
                """.trimIndent())
            }

            if (safeTagLimit < 1) {
                throw IllegalArgumentException("""
                    Using a prefix of '$prefix', and hard tag limit length of `$hardSafeTagLimit`,
                    will result in empty/illegal tag value.
                """.trimIndent())
            }
        }

        override fun rewriteTag(tag: String): String {

            var ts = tag.trim()

            if (ts.length > safeTagLimit) {
                ts = DropVowelsToShorten.rewriteTag(tag)
            }

            if (ts.length > safeTagLimit) {
                ts = ts.substring(ts.length - safeTagLimit)
            }

            return ts
        }

        private fun prefix(tag: String) = "$prefix$tag".trim()
    }


    companion object {

        /**
         * Android limit for tags are generally 23. Later SDK lifts this restriction, but better play it safe.
         */
        const val SDK_SAFE_TAG_LIMIT = 23


    }
}




