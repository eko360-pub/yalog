package org.andriesfc.yalog.config

import org.andriesfc.yalog.internal.Predicate
import org.andriesfc.yalog.internal.acceptsByAll

/**
 * Allow the top level configuration to reject messages with specific tags.
 */
interface TagFilter {
    fun accept(tag: String): Boolean
}


fun TagFilter.toPredicate(): Predicate<String> = { tag: String -> accept(tag) }


fun tagFilter(acceptTag: (String) -> Boolean): TagFilter = object : TagFilter {
    override fun accept(tag: String): Boolean {
        return acceptTag(tag)
    }
}

fun List<TagFilter>.rejectedByNone() = map { it.toPredicate() }.run {
    tagFilter {
        acceptsByAll(it, true)
    }
}