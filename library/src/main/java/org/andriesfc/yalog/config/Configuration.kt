package org.andriesfc.yalog.config

import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.message.LogMessage
import org.andriesfc.yalog.message.MessageFactory
import org.andriesfc.yalog.processing.Distributor

/**
 * A basic DSL driver for configuring the underlying logging machinery.
 */
interface Configuration {
    fun addAppender(appender: LogAppender)
    fun addTagFilter(tagFilter: TagFilter)
    fun filterOnLevel(levelFilter: LevelFilter)
    fun addMessageFilter(messageFilter: LogMessageFilter)
    fun rewriteTagsWith(tagRewriteOperation: TagRewriteOperation)
    fun distributeMessagesWith(distributor: Distributor)
    fun produceLogMessagesWith(messageFactory: MessageFactory)
    fun defaults()
    fun clear()
}

operator fun Configuration.plusAssign(messageAppender: LogAppender) = addAppender(messageAppender)
operator fun Configuration.plusAssign(tagFilter: TagFilter) = addTagFilter(tagFilter)
operator fun Configuration.plusAssign(levelFilter: LevelFilter) = filterOnLevel(levelFilter)
operator fun Configuration.plusAssign(messageFilter: LogMessageFilter) = addMessageFilter(messageFilter)

fun Configuration.rewriteTagsWith(rewrite:(old:String) -> String) {
    rewriteTagsWith(rewriteTag { rewrite(it) })
}

fun Configuration.addAppender(appendMessage: (LogMessage) -> Unit) {
    addAppender(object : LogAppender {
        override fun append(message: LogMessage) {
            appendMessage(message)
        }
    })
}

fun Configuration.addTagFilter(filter: (String) -> Boolean) {
    addTagFilter(object : TagFilter {
        override fun accept(tag: String): Boolean {
            return filter(tag)
        }
    })
}

fun Configuration.setLeastLevel(acceptLevel: Level) {
    filterOnLevel { level -> level shouldAtLeastBeAllowed(acceptLevel) }
}

fun Configuration.filterOnLevel(acceptLevel: (Level) -> Boolean) {
    this@filterOnLevel.filterOnLevel(object : LevelFilter {
        override fun accept(level: Level): Boolean {
            return acceptLevel(level)
        }
    })
}
