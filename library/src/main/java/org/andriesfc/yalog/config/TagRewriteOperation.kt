package org.andriesfc.yalog.config

/**
 * A function which can be used to rewrite tags before they get distributed to log appender instances.
 */
interface TagRewriteOperation {
    fun rewriteTag(tag: String): String
}

fun rewriteTag(rewrite: (String) -> String): TagRewriteOperation {
    return object : TagRewriteOperation {
        override fun rewriteTag(tag: String): String {
            return rewrite(tag)
        }
    }
}

fun List<TagRewriteOperation>.chainHandlers(): TagRewriteOperation {
    return when {
        isEmpty() -> BuiltInTagRewriteOperation.KeepAsIs
        size == 1 -> first()
        else -> rewriteTag { initial -> fold(initial) { tag, r -> r.rewriteTag(tag) } }
    }
}