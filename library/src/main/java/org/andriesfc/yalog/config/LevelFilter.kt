package org.andriesfc.yalog.config

import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.internal.Predicate

/**
 * A filter which can be used to apply global filter on messages.
 */
interface LevelFilter {
    fun accept(level: Level): Boolean
}

fun LevelFilter.toPredicate(): Predicate<Level> = { level -> accept(level) }
fun levelFilter(acceptLevel: (Level) -> Boolean): LevelFilter = object : LevelFilter {
    override fun accept(level: Level): Boolean {
        return acceptLevel(level)
    }
}

fun List<LevelFilter>.rejectedByNone(): LevelFilter {

    fun rank(f: LevelFilter): Int = with(f) {
        Level.values()
                .asSequence()
                .filter(this::accept)
                .sortedBy(Level::priority)
                .firstOrNull()?.priority
                ?: Int.MAX_VALUE
    }

    return if (isEmpty()) levelFilter {
        true
    } else {

        val ranked = sortedBy { rank(it) }

        levelFilter { level ->
            !ranked.any { !it.accept(level) }
        }
    }
}