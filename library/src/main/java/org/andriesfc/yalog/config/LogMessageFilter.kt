package org.andriesfc.yalog.config

import org.andriesfc.yalog.internal.Predicate
import org.andriesfc.yalog.internal.acceptsByAll
import org.andriesfc.yalog.message.LogMessage

/**
 * A filter which can be used to filter complete messages, and prevent them from being
 * distributed to [org.andriesfc.yalog.appender.LogAppender]s.
 */
interface LogMessageFilter {
    fun accept(logMessage: LogMessage): Boolean
}

fun LogMessageFilter.toPredicate(): Predicate<LogMessage> = { message -> accept(message) }
fun messageFilter(acceptMessage: (LogMessage) -> Boolean): LogMessageFilter {
    return object : LogMessageFilter {
        override fun accept(logMessage: LogMessage): Boolean {
            return acceptMessage(logMessage)
        }
    }
}

fun List<LogMessageFilter>.rejectedByNone() = map { it.toPredicate() }.run {
    messageFilter {
        acceptsByAll(it, true)
    }
}