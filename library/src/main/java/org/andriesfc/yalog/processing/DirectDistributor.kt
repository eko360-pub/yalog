package org.andriesfc.yalog.processing

import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.message.LogMessage

/**
 * Just post the message to each selected appender directly.
 */
class DirectDistributor : Distributor {
    override fun distribute(message: LogMessage, selection: List<LogAppender>) {
        selection.onEach { it.append(message) }
    }
}