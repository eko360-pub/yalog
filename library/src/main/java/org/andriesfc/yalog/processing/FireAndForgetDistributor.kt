package org.andriesfc.yalog.processing

import kotlinx.coroutines.experimental.*
import org.andriesfc.yalog.Log
import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.message.LogMessage
import kotlin.coroutines.experimental.CoroutineContext


/**
 * Assume each appender has to do I/O, and schedules it to run
 * asynchronously on the [Dispatchers.IO] context. logging is a fire-and-forget
 * scenario here.
 */
class FireAndForgetDistributor : Distributor, CoroutineScope {

    private lateinit var job: Job

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    override fun distribute(message: LogMessage, selection: List<LogAppender>) {
        launch {
            for (a in selection) {
                launch {
                    try {
                        a.append(message)
                    } catch (e: Exception) {
                        Log.e(TAG, e, "Error sending message to {} to {}", message, a)
                    }
                }
            }
        }
    }

    override fun start() {
        job = Job()
    }

    override fun stop() {
        job.cancel()
    }

    companion object {
        private const val TAG = "FireAndForgetDist"
    }
}

