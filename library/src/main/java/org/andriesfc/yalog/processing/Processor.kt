package org.andriesfc.yalog.processing

import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.config.LogMessageFilter
import org.andriesfc.yalog.message.LogMessage
import org.andriesfc.yalog.message.MessageFactory
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Processor is responsible for the ongoing processing of log messages.
 *
 * The processing includes the following responsibilities:
 *
 * 1. Ensure that all global filters gets applied before distributing an actual message to any appender - only do potential I/O if required.
 * 2. Start and stop all appenders.
 * 3. Ensure there
 *
 * @param version The version of the processor. Each configuration will increase this value.
 * @param availableDestinations A list of possible [Destination]s.
 * @param messageFactory A factory which allows it create [LogMessage]s.
 * @param globalMessageFilter A global message filter which prevent the distribution of an incoming message.
 * @param beforeStarting A function to indicate that it ready to replace the existing processor (if any).
 *
 * @see invoke
 */
class Processor(val version: Int,
                configuredDistributor: Distributor,
                private val availableDestinations: List<Destination>,
                private val messageFactory: MessageFactory,
                private val globalMessageFilter: LogMessageFilter): Stoppable {

    private val started = AtomicBoolean(false)
    private val lock = 1

    override fun toString(): String = """
        Processor {
            version = $version;
            availableDestinations = ${availableDestinations.joinToString(
            prefix = "[",
            postfix = "]",
            separator = "\n\t"
    ) { "$it" }};
            messageFactory = $messageFactory;
            globalMessageFilter = $globalMessageFilter
        }
    """.trimIndent()

    private val distributor = object : Distributor by configuredDistributor {
        override fun distribute(message: LogMessage, selection: List<LogAppender>) {
            if (!started.get()) throw IllegalStateException("Processor has been stopped.")
            configuredDistributor.distribute(message, selection)
        }
    }


    /**
     * Invokes this processor with all the data available for processing.
     *
     * > **NOTE**:
     * > Filters needs to reject a logging request as quick as possible
     */
    operator fun invoke(tag: String, level: Level, cause: Throwable?, message: String, args: Array<out Any?>) {

        val next: LogMessage by lazy { messageFactory.createMessage(level, tag, cause, message, args) }
        val rejected by lazy { !globalMessageFilter.accept(next) }

        val distribution = mutableListOf<LogAppender>().apply {
            for (destination in availableDestinations) {
                val appender = destination.select(level, tag) ?: continue
                if (rejected) {
                    clear()
                    break
                } // stop distribution of this message
                this += appender
            }
        }

        if (rejected || distribution.isEmpty()) {
            return
        } else {
            distributor.distribute(next, distribution)
        }

    }

    /**
     * Replaces it self via the [beforeStarting] function reference, and stops the old one before
     * starting itself.
     */
    internal fun start() {
        if (started.compareAndSet(false, true)) {

            val r = synchronized(lock) {
                try {
                    distributor.start()
                    availableDestinations.onEach(Destination::start)
                    null
                } catch (e: Exception) {
                    e
                }
            }

            if (r != null) {
                stop()
                throw r
            }
        }
    }

    override fun stop() {
        if (!started.get()) return
        synchronized(lock) {
            // Note order: availableDestinations must be stopped before distributor
            availableDestinations.onEach(Destination::stop)
            distributor.stop()
            started.set(false)
        }
    }

}
