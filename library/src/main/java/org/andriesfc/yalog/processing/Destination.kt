package org.andriesfc.yalog.processing

import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.appender.logAppender
import org.andriesfc.yalog.appender.toTransformingAppender
import org.andriesfc.yalog.config.LevelFilter
import org.andriesfc.yalog.config.TagFilter
import org.andriesfc.yalog.config.TagRewriteOperation
import org.andriesfc.yalog.internal.id
import org.andriesfc.yalog.message.LogMessage

/**
 * Destination for log messages.
 *
 * Fulfills 3 responsibilities:
 *
 * 1. Query of this destination has an appropriate [LogAppender] to receive a given tag on a specific level.
 * 2. Applies any formatting and pre-processing required by the appender before receiving the message.
 * 3. Exposes any process control ([Processing] API) the appender may implement.
 *
 * @param tagFilter Filter as configured.
 * @param tagRewriteOperation as configured.
 * @param levelFilter Filter as configured.
 * @param target The configured log appender.
 */
class Destination(private val target: LogAppender,
                  private val tagFilter: TagFilter,
                  private val levelFilter: LevelFilter,
                  private val tagRewriteOperation: TagRewriteOperation?) : Processing {

    private val selectable = logAppender { messageIn ->

        val message = when(tagRewriteOperation) {
            null -> messageIn
            else -> object : LogMessage by messageIn {
                override val tag: String = tagRewriteOperation.rewriteTag(messageIn.tag)
            }
        }

        target.append(message)
    }

    /**
     * Selects configured [LogAppender] if configuration allows.
     */
    fun select(level: Level, tag: String): LogAppender? {
        return when {
            !levelFilter.accept(level) -> null
            !tagFilter.accept(tag) -> null
            else -> selectable
        }
    }


    /**
     * Starts the appender if implements the [Processing] API.
     */
    override fun start() {
        target as? Processing ?: return
        target.start()
    }

    /**
     * Stops the appender if implements the [Processing] API.
     */
    override fun stop() {
        target as? Processing ?: return
        target.stop()
    }

    override fun toString(): String {
        return """
            Destination {
                id = "${id()}";
                tagFilter = { id="${tagFilter.id()}" };
                levelFilter = { id="${levelFilter.id()}" };
                tagRewriteFunction = $tagRewriteOperation
            }
        """.trimIndent()
    }
}