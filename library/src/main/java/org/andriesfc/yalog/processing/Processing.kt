package org.andriesfc.yalog.processing

/**
 * A component which can only be stopped. The assumption here is that whatever function produces
 * it, is responsible to start it.
 */
interface Stoppable {
    fun stop()
}

/**
 * A component which can started, and stopped.
 */
interface Processing : Stoppable {
    fun start()
}