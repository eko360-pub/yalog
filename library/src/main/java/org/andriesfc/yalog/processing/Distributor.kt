package org.andriesfc.yalog.processing

import org.andriesfc.yalog.appender.LogAppender
import org.andriesfc.yalog.message.LogMessage

/**
 * Distributes a message to a selection of [LogAppender]s.
 *
 * If the distributor needs to know when it gets started, and stopped, it should
 * override the [start] and [stop] default implementations (which is an no-op).
 *
 * @see distribute
 */
interface Distributor : Processing  {

    /**
     * Distributes a list of [LogMessage] over [selection] of [LogAppender] instances.
     *
     * @param message The message to distribute.
     * @param selection A selection of log appender instances which should receive it.
     */
    fun distribute(message: LogMessage, selection:List<LogAppender>)

    /**
     * Starts the distributor.
     */
    override fun start() = Unit

    /**
     * Stops the distributor.
     */
    override fun stop() = Unit
}

