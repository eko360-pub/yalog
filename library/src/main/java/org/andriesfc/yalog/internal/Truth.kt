package org.andriesfc.yalog.internal

typealias Predicate<T> = (T) -> Boolean

fun <T> List<Predicate<T>>.rejects(item: T, emptyCase: Boolean = false): Boolean {
    return if (isEmpty())
        emptyCase
    else
        firstOrNull { accepts -> !accepts(item) } != null
}

fun <T> List<Predicate<T>>.acceptsByAll(item: T, emptyCase: Boolean) : Boolean {
    return if (isEmpty()) {
        emptyCase
    } else {
        fold(true) { acc, accept -> acc && accept(item) }
    }
}