package org.andriesfc.yalog.internal

import java.io.PrintWriter
import java.io.StringWriter

operator fun StringBuilder.plusAssign(item: Any?) {
    append(item)
}

fun StringBuilder.printed(writeOut: PrintWriter.(StringBuilder) -> Unit) {
    StringWriter().apply {
        PrintWriter(this).apply {
            writeOut(this@printed)
            flush()
            close()
        }
        append(buffer.toString())
    }
}

fun printed(print: PrintWriter.(StringBuilder) -> Unit) = buildString { printed(print) }


fun String.couldBeTrimmed(): Boolean = isNotEmpty() && first().isWhitespace() || last().isWhitespace()

