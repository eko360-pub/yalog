package org.andriesfc.yalog.internal

fun Any?.id():String? = this?.hashCode()?.toString(16)