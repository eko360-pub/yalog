package org.andriesfc.yalog.internal

import java.io.File
import java.io.OutputStream

fun File.copyTo(read: ReadInstruction, out: OutputStream): Long {
    return inputStream().use { fileStream ->

        val write = fun (byteArray: ByteArray, bytes: Int) {
            if (bytes > 0) {
                out.write(byteArray, 0, bytes)
            }
        }

        when (read) {
            ReadInstruction.All -> {
                fileStream.copyTo(out)
            }
            is ReadInstruction.Head -> {
                val bytes = ByteArray(read.size)
                fileStream.read(bytes).also { actual -> write(bytes, actual) }.toLong()
            }
            is ReadInstruction.Tail -> {
                val length = length()
                val bytes = ByteArray(read.size)
                fileStream.skip(if (length > read.size.toLong())
                    length - read.size
                else
                    0
                )
                fileStream.read(bytes).also { actual -> write(bytes, actual) }.toLong()
            }
        }
    }
}