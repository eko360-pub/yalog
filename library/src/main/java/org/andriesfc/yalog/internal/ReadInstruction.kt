package org.andriesfc.yalog.internal

sealed class ReadInstruction {
    object All : ReadInstruction()
    class Head(val size: Int) : ReadInstruction()
    class Tail(val size: Int) : ReadInstruction()
}