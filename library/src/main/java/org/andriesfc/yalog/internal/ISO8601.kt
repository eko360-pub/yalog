@file:JvmName("ISO8601") package org.andriesfc.yalog.internal

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Date.toISO8601(): String {
    val formatted = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(this)
    return  "${formatted.substring(0, 22)}:${formatted.substring(22)}"
}

fun now() = Date().toISO8601()

const val ISO_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ"
