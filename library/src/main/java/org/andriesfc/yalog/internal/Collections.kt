package org.andriesfc.yalog.internal

fun <E, C : Collection<E>> C.unique(collect: (E) -> Unit) {
    val seen = mutableSetOf<E>()
    try {
        for (item in this) {
            if (seen.add(item)) {
                collect(item)
            }
        }
    } finally {
        seen.clear()
    }
}

fun <E> List<E>.unique(): List<E> {
    return when {
        this.isEmpty() -> this
        this.size == 1 -> this
        else -> ArrayList<E>(size).also { collected -> unique { collected + it } }.toList()
    }
}

fun <E> MutableCollection<E>.addOnce(item: E) {
    if (item !in this) {
        add(item)
    }
}