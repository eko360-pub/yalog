package org.andriesfc.yalog.internal

import android.content.Context
import android.content.pm.ApplicationInfo

inline val Context.isDebuggable : Boolean get() = applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE != 0


