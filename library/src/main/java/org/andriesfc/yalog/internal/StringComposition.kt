package org.andriesfc.yalog.internal


private val rePlaceHolders = Regex("\\{(\\d*)\\}")


private inline val MatchResult.start: Int get() = range.start
private inline val MatchResult.endInclusive: Int get() = range.endInclusive


fun <T> String.decompose(valueOfPlaceHolder: (Int) -> Any?, toPart: (value: Any?) -> T): List<T?> {


    val parts = mutableListOf<T?>()

    var nextArg = 0

    val last = rePlaceHolders.findAll(this).fold(0) { last, match ->

        // Add part we skipped over to this placeholder
        parts += toPart(substring(last, match.start))

        val (x) = match.destructured
        val replacementIndex = if (x.isEmpty()) nextArg++ else x.toInt() - 1
        val replacement = toPart(valueOfPlaceHolder(replacementIndex))
        parts += replacement

        match.endInclusive + 1
    }

    if (last < length) {
        parts += toPart(substring(last))
    }


    return parts.toList()

}

private val IDENTITY = { x: Any? -> x }

@Suppress("UNCHECKED_CAST")
private fun <T> identity() = IDENTITY as ((T) -> T)

private fun <T> List<T>.getOrRestorePlaceholder() = { index: Int -> getOrNull(index) ?: "{${index + 1}}" }

fun String.compose(args: List<Any?>) : String {
    return decompose(args.getOrRestorePlaceholder(), identity())
            .asSequence()
            .map { "$it" }
            .joinToString(separator = "")
}