package org.andriesfc.yalog.message.part

import org.andriesfc.yalog.message.part.Part.ValuePart

open class PartFactory  {

    open fun <T> partOf(value: T, clazz: Class<T>): ValuePart<T> = TypedPart.Value(value, clazz)

    open fun <T> modifiablePartOf(value: T, clazz: Class<T>) : Part.ModifiableValuePart<T> = TypedPart.Modifiable(value, clazz)

    open fun nullPart(): Part.ModifiableValuePart<Any?> = nullablePart(null as Any?)

    companion object {

        @Suppress("UNCHECKED_CAST")
        private inline fun <reified T> nullablePart(initial: T? = null): Part.ModifiableValuePart<T?> {
            val nullableType: Class<T?> = T::class.java as Class<T?>
            return TypedPart.Nullable(initial, nullableType)
        }

    }
}

inline fun <reified T> PartFactory.partOf(value: T) = partOf(value, T::class.java)
inline fun <reified T> PartFactory.modifiablePartOf(initial: T) = modifiablePartOf(initial, T::class.java)



