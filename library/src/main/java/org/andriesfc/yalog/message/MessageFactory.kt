package org.andriesfc.yalog.message

import org.andriesfc.yalog.appender.Level

interface MessageFactory {
    fun createMessage(
            level: Level,
            tag: String,
            cause: Throwable?,
            message: String,
            args:Array<out Any?>
    ): LogMessage
}

