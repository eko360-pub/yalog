package org.andriesfc.yalog.message


/**
 * A interface to indicate that something can rendered out unto a specific output handler.
 *
 * @see TextRenderable
 */
@Suppress("SpellCheckingInspection")
interface Renderable<T> {
    fun render(out: T)
}

