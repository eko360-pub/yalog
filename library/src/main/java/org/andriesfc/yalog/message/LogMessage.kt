package org.andriesfc.yalog.message

import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.message.part.Part
import org.andriesfc.yalog.message.part.PartContainer

/**
 * Describes the shape of message being submitted to for distribution to various log appender instances.
 */
interface LogMessage : Part {
    /**
     * ID of the message - should be unique.
     */
    val id: String
    /**
     * When the message was generated on the device.
     */
    val timestamp: ZonedTimestamp
    /**
     * At what level was it submitted.
     */
    val level: Level
    /**
     * On which tag was it submitted.
     */
    val tag: String
    /**
     * What was the cause? Maybe `null` if no fault occurred.
     */
    val cause: Throwable?
    /**
     * The different parts of the message. **_Note_**, it is by design that the parts can be modified
     * via the [PartContainer].
     */
    val parts: PartContainer
}

