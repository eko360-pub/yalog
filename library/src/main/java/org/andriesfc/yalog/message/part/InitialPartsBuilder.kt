package org.andriesfc.yalog.message.part

import org.andriesfc.yalog.internal.decompose


fun  PartFactory.build(message: String, args: List<Any?>) : MutableList<Part> {

    fun valueOfPlaceholder(index: Int): Any? = args.getOrElse(index) { "{$index}" }

    return message.decompose(::valueOfPlaceholder) {
        when (it) {
            null -> nullPart()
            else -> modifiablePartOf(it, it.javaClass)
        }
    }.map {
        @Suppress("UNCHECKED_CAST")
        it as TypedPart<Any?>
    }.toMutableList()


}
