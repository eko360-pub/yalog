@file:Suppress("MemberVisibilityCanBePrivate")

package org.andriesfc.yalog.message.part

import org.andriesfc.yalog.message.TextRenderable
import java.util.*

typealias KeyType = String

/**
 * Represents a part of message, or even a complete message. By default
 * parts are text renderable (via the render function which renders out to [StringBuilder]
 * instance).
 */
interface Part : TextRenderable {

    /**
     * Holds a specific [value] with a specific [valueType]
     */
    interface ValuePart<T> : Part {
        val value: T
        val valueType: Class<T>
    }

    /**
     * Holds a specific [value] which can be changed post message construction.
     */
    interface ModifiableValuePart<T> : ValuePart<T> {
        override var value: T
    }

    /**
     * A special part which can detached (it is part of another).
     */
    interface DetachablePart : Part {
        val detached: Boolean
        fun detach()
    }

    /**
     * References part which was attached to another part. Used to enrich message
     * parts with more data and enhance the downstream rendering and/or logging.
     */
    interface Attachment<T> : DetachablePart, ModifiableValuePart<T> {
        val attachedTo: Part?
        val key: KeyType
    }

    /**
     * Attaches a value with given key to this part.
     *
     * @param key The key to retrieve the attachment with.
     * @param valueType The [Class] of the value
     * @param value The actual value.
     */
    fun <T> attach(key: KeyType, valueType: Class<T>, value: T): Attachment<T>


    /**
     * A map of attachments retrieval by keys.
     */
    val attachments: Map<KeyType, Attachment<Any?>>

}

/**
 * Attaches a value with a given key
 */
inline fun <reified T> Part.attach(key: KeyType, value: T): Part.Attachment<T> = attach(key, T::class.java, value)

/**
 * Attaches a value with auto generated, (using the [UUID.randomUUID] function) as key.
 */
inline fun <reified T> Part.attach(value: T): Part.Attachment<T> = attach("${UUID.randomUUID()}", T::class.java, value)

/**
 * Query all attachments which a holds a value specific type of [Part.ValuePart].
 */
@Suppress("UNCHECKED_CAST")
inline fun <reified T> Map<KeyType, Part.Attachment<Any>>.ofType(): List<Part.Attachment<T>> =
        values.asSequence().filter { it.value is T }.toList() as List<Part.Attachment<T>>

/**
 * Query for a given attachment part with a given type.
 *
 * @return Either the [Part.Attachment], or null of it does not exists, or the value is not of
 * desired type.
 */
@Suppress("UNCHECKED_CAST")
inline fun <reified T> Map<KeyType, Part.Attachment<Any>>.ofType(key: KeyType): Part.Attachment<T>? {
    return (get(key)?.value as? T)?.let { it as Part.Attachment<T> }
}

fun List<Part>.combine() = buildString {
    for (part in this@combine) {
        part.render(this)
    }
}
