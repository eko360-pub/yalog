package org.andriesfc.yalog.message.part

import android.provider.Telephony

/**
 * Limited container for parts: Parts can be replaced, but not removed, or added, the only
 * exception being a [Part.DetachablePart] which has previously added to via the [BasePart] -
 * calling [Part.DetachablePart.detach] will remove it from the part list.
 */
interface PartContainer : Part, List<Part> {
    operator fun set(index: Int, part: Part)
}

inline fun <reified T, reified P : Part.ValuePart<out T>> PartContainer.select(noinline predicate: (Part.ValuePart<out T>) -> Boolean = { true }) = asSequence()
        .filterIsInstance(P::class.java)
        .map { part -> part as Part.ValuePart<out Any?> }
        .filter { part -> part.value is T }
        .map { part -> part as P }
        .filter(predicate)

inline fun <reified T> PartContainer.selectAttachments(noinline predicate: (Part.Attachment<out T>) -> Boolean = { true }): Sequence<Part.Attachment<T>> =
        attachments.asSequence()
                .filter { (_,a) -> a.value is T }
                .map { (_,a) ->
                    @Suppress("UNCHECKED_CAST")
                    a as Part.Attachment<T>
                }.filter(predicate)
