package org.andriesfc.yalog.message

import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.message.part.*


class DefaultMessageFactory(private val partFactory: PartFactory = PartFactory()) : MessageFactory {
    override fun createMessage(level: Level, tag: String, cause: Throwable?, message: String, args: Array<out Any?>): LogMessage {
        return DefaultLogMessage(
                tag = tag,
                level = level,
                cause = cause,
                parts = partFactory.build(message, args.toList()).toContainer()
        )
    }
}

