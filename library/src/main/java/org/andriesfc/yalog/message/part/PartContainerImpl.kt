package org.andriesfc.yalog.message.part

import java.util.*

private class PartContainerImpl(private val parts: MutableList<Part>) : List<Part> by parts, BasePart(), PartContainer {

    override fun render(out: StringBuilder) = iterator().forEach { it.render(out) }
    override fun partDetached(part: Part) {
        parts.remove(part)
    }

    override fun set(index: Int, part: Part) {
        parts[index] = part
    }

    override fun equals(other: Any?): Boolean {
        return when {
            other === this -> true // Here be dragons: Must use `===` to avoid circular call to equals()
            other is PartContainerImpl -> other.parts == parts
            else -> false
        }
    }

    override fun hashCode(): Int = Objects.hash(parts.toTypedArray())

}


fun MutableList<Part>.toContainer() : PartContainer = PartContainerImpl(this)