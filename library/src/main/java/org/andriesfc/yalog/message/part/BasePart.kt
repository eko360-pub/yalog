package org.andriesfc.yalog.message.part

import org.andriesfc.yalog.internal.plusAssign
import org.andriesfc.yalog.message.part.BasePart.DefaultAttachment
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicReference

/**
 * A base part to extend from building other [Part] implementations.
 *
 * Extending from it provides the following:
 *
 * - A [DefaultAttachment] implementation, which is open for extension.
 * - The following extension points:
 *
 *    1. [createAttachment] to implement your own attachment type.
 *    2. [partDetached] to indicate that a part has been detached.
 */
abstract class BasePart : Part {

    private val _attachments = ConcurrentHashMap<KeyType, Part.Attachment<Any?>>()

    override val attachments: Map<KeyType, Part.Attachment<Any?>>
        get() = _attachments

    @Suppress("UNCHECKED_CAST")
    final override fun <T> attach(key: KeyType, valueType: Class<T>, value: T): Part.Attachment<T> {

        val detachMe = { part: Part ->
            val removed = _attachments.remove(key) != null
            if (removed) {
                partDetached(part)
            }
        }

        val attachment = createAttachment(
                detachMe = detachMe,
                valueType = valueType,
                value = value,
                key = key)

        _attachments.remove(key)?.detach()
        _attachments[key] = attachment as Part.Attachment<Any?>

        return attachment
    }

    protected open fun partDetached(part: Part) = Unit

    /**
     * Override this to sub class the [DefaultAttachment], or replace the implementation
     * of with another.
     */
    @Suppress("MemberVisibilityCanBePrivate")
    protected open fun <T> createAttachment(
            detachMe: (Part) -> Unit,
            valueType: Class<T>,
            value: T,
            key: KeyType
    ): Part.Attachment<T> = DefaultAttachment(this, key, value, valueType, detachMe)

    /**
     * An extensible [Part.Attachment] implementation which can be used as base class.
     *
     * This implementation provides only one extension point, [detached] which
     * the implementer can override to know once it has been
     * removed from the containing part.
     *
     * @param owner The owner part.
     * @param key The key for this attachment.
     * @param value A value it holds.
     * @param valueType The type of value it holds.
     * @param detachMe A function which gets invoked when the [detach] function is called.
     */
    protected open class DefaultAttachment<T>(
            owner: Part,
            override val key: KeyType,
            value: T,
            valueType: Class<T>,
            private val detachMe: (Part) -> Unit
    ) : Part.ModifiableValuePart<T> by TypedPart.Modifiable(value, valueType), Part.Attachment<T> {

        private val _owner = AtomicReference(owner)

        final override val attachedTo: Part?
            get() = _owner.get()

        final override val detached: Boolean
            get() = attachedTo != null

        final override fun detach() {
            val removed = _owner.getAndSet(null)?.also(detachMe) != null
            if (removed) detached()
        }

        protected open fun detached() = Unit

        override fun render(out: StringBuilder) {
            out += """
                |Attachment {
                |   key="$key",
                |   type="${javaClass.name}",
                |   owner = {$_owner},
                |   valueType = "${valueType.name}",
                |   value = "$value"
                |   attachments = ${attachments.map { (_,a) -> "$a" }}
                |}
            """.trimMargin()
        }

        override fun toString(): String = buildString { render(this) }
    }
}


