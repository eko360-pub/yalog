package org.andriesfc.yalog.message

import android.annotation.SuppressLint
import org.andriesfc.yalog.internal.ISO_PATTERN
import java.text.SimpleDateFormat
import java.util.*

enum class Month(val field: Int) {
    JAN(0),
    FEB(1),
    MAR(2),
    APR(3),
    MAY(4),
    JUN(5),
    JUL(6),
    AUG(7),
    SEP(8),
    OCT(9),
    NOV(10),
    DEC(11)
}

class ZonedTimestamp(
        val timestamp: Long,
        val timeZone: TimeZone = TimeZone.getDefault())
    : Comparable<ZonedTimestamp> {


    constructor() : this(System.currentTimeMillis())
    constructor(calendar: Calendar) : this(calendar.timeInMillis, calendar.timeZone)
    constructor(year: Int, month: Month, date: Int, timeZone: TimeZone = TimeZone.getDefault()) : this(Calendar.getInstance(timeZone).apply {
        set(Calendar.YEAR, year)
        set(Calendar.MONTH, month.field)
        set(Calendar.DATE, date)
        atStartOfDay()
    })

    constructor(year: Int, month: Month, date: Int, hour:Int, minute: Int, second:Int, timeZone: TimeZone = TimeZone.getDefault()): this(Calendar.getInstance(timeZone).apply {
        set(Calendar.YEAR, year)
        set(Calendar.MONTH, month.field)
        set(Calendar.DATE, date)
        set(Calendar.HOUR, hour)
        set(Calendar.MINUTE, minute)
        set(Calendar.SECOND, second)
        set(Calendar.MILLISECOND, 0)
    })

    @SuppressLint("SimpleDateFormat")
    override fun toString(): String = SimpleDateFormat(ISO_PATTERN).run {
        timeZone = this@ZonedTimestamp.timeZone
        format(Date(timestamp))
    }

    override fun compareTo(other: ZonedTimestamp): Int {
        return when {
            other === this -> 0
            other.timeZone == timeZone -> timestamp.compareTo(other.timestamp)
            else -> {
                val otherAtSameTZ = other.atTimeZone(timeZone)
                timestamp.compareTo(otherAtSameTZ.timestamp)
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        return  when {
            other === this -> true
            other is ZonedTimestamp -> compareTo(other) == 0
            else -> false
        }
    }

    override fun hashCode(): Int = Objects.hash(timeZone, timestamp)
}


fun ZonedTimestamp.calender(): Calendar = Calendar.getInstance(timeZone).apply { timeInMillis = timestamp }
fun ZonedTimestamp.date(): Date = calender().time
fun ZonedTimestamp.atTimeZone(anotherTimeZone: TimeZone): ZonedTimestamp {
    return when (timeZone) {
        anotherTimeZone -> this
        else -> ZonedTimestamp(calender().also { it.timeZone = anotherTimeZone })
    }
}

fun Calendar.atStartOfDay(): Calendar = apply {
    fun setMin(field: Int) = set(field, getMinimum(field))
    arrayOf(Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND).forEach(::setMin)
}

fun tz(id: String): TimeZone = TimeZone.getTimeZone(id)



