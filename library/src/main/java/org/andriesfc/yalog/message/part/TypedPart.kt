package org.andriesfc.yalog.message.part

import org.andriesfc.yalog.message.part.Part.ModifiableValuePart

sealed class TypedPart<T>(override val value: T, override val valueType: Class<T>) : BasePart(), Part.ValuePart<T> {

    override fun render(out: StringBuilder) {
        out.append(value)
    }

    data class Value<T>(override val value: T, override val valueType: Class<T>) : TypedPart<T>(value, valueType)

    data class Modifiable<T>(override var value: T, override val valueType: Class<T>) : TypedPart<T>(value, valueType), ModifiableValuePart<T>

    data class Nullable<T>(override var value: T? = null, override val valueType: Class<T?>) : TypedPart<T?>(value, valueType), ModifiableValuePart<T?>

}
