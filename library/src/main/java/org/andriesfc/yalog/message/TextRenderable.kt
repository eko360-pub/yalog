package org.andriesfc.yalog.message

/**
 * Indicates that part or something knows how to render itself to [StringBuilder], which is simple
 * text.
 */
@Suppress("SpellCheckingInspection")
interface TextRenderable : Renderable<StringBuilder>

fun TextRenderable.render(): String = buildString { render(this) }