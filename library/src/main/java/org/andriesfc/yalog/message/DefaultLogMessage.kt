package org.andriesfc.yalog.message

import org.andriesfc.yalog.appender.Level
import org.andriesfc.yalog.message.part.BasePart
import org.andriesfc.yalog.message.part.PartContainer
import java.util.*

internal data class DefaultLogMessage(
        override val id: String = "${UUID.randomUUID()}",
        override val tag: String,
        override val level: Level,
        override val cause: Throwable?,
        override val timestamp: ZonedTimestamp = ZonedTimestamp(),
        override val parts: PartContainer
) : BasePart(), LogMessage {
    override fun render(out: StringBuilder) = parts.render(out)
}
