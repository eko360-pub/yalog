package org.andriesfc.yalog.appender

import org.andriesfc.yalog.config.Contributor
import org.andriesfc.yalog.config.TagRewriteOperation

/**
 * Interface that indicate that a log appender wants the tags to be formatted before
 * receiving the actual message.
 */
interface TagRewriteContributor : Contributor<LogAppender> {
    val tagRewriteContribution: TagRewriteOperation
}