package org.andriesfc.yalog.appender

import org.andriesfc.yalog.appender.AndroidLogAppender.Companion.DEFAULT_CONTINUE_MESSAGE
import org.andriesfc.yalog.appender.AndroidLogAppender.Companion.MAX_LOG_LINE_MESSAGE_WIDTH
import org.andriesfc.yalog.appender.Level.*
import org.andriesfc.yalog.config.BuiltInTagRewriteOperation
import org.andriesfc.yalog.config.TagRewriteOperation
import org.andriesfc.yalog.internal.now
import org.andriesfc.yalog.internal.plusAssign
import org.andriesfc.yalog.message.LogMessage
import org.andriesfc.yalog.message.part.combine
import android.util.Log as _androidLogger


/**
 * Android Log Appender
 *
 * In addition of sending messages to the underlying [android.util.Log], this implementation also
 * provides the following:
 *
 * 1. Excessive long message are split across multiple log messages.
 * 2. Each part will have an human friendly, (but configurable), [continueOnNextMessage] suffix at the end.
 * 3. Tags will be rewritten based on the [BuiltInTagRewriteOperation.SdkSafe]
 *
 * **_Note_**: The class is open for extension in the following manner:
 *
 * Overriding the [splitLargeMessage] function allows you control over the splitting of a single
 * text message into multiple pieces.
 *
 * If you want to control what text gets returned from the [LogMessage.parts], override the
 * [compose] function, and provide your own implementation.
 *
 * If the [BuiltInTagRewriteOperation.SdkSafe] is not to your liking, you may also
 * override the [tagRewriteContribution] property, and provide your own.
 *
 * @param maxAllowedMessageLength The maximum allowed message length, defaults to [MAX_LOG_LINE_MESSAGE_WIDTH]
 * @param continueOnNextMessage A message to indicate the next piece of the overall message will be in the next log message. Defaults to [DEFAULT_CONTINUE_MESSAGE]
 */
@Suppress("MemberVisibilityCanBePrivate")
open class AndroidLogAppender(
        maxAllowedMessageLength: Int = MAX_LOG_LINE_MESSAGE_WIDTH,
        val continueOnNextMessage: String = DEFAULT_CONTINUE_MESSAGE
) : LogAppender, TagRewriteContributor {

    val sizeLimitPerMessage = maxAllowedMessageLength - continueOnNextMessage.length

    private val timestamp = now()

    var appending: Boolean = false
        private set

    final override fun append(message: LogMessage) {

        if (message.level == OFF) {
            return
        }

        appending = true

        try {

            with(message) {

                val content = compose(this)

                if (content.length <= sizeLimitPerMessage) {
                    writeOutAndroidLog(tag, level, cause, content)
                } else splitLargeMessage(content) { part, isLast ->
                    val loggableCause = if (isLast) cause else null
                    val loggableMessage = if (isLast) part else "$part$continueOnNextMessage"
                    writeOutAndroidLog(tag, level, loggableCause, loggableMessage)
                }
            }

        } finally {
            appending = false
        }
    }

    /**
     * Splits a message into sentences no more than the [maxMessageSize]
     *
     * @param content The message to splitLargeMessage.
     * @param emitPart A function which receives the sentence, and and indication that it the last.
     */
    protected open fun splitLargeMessage(content: String, emitPart: (messagePart: String, isLast: Boolean) -> Unit) {

        val words = content.split(Regex("\\s+")).toMutableList()

        while (words.isNotEmpty()) {

            val messagePart = buildString {
                while (words.isNotEmpty() && (length + words.first().length < sizeLimitPerMessage)) {
                    if (length > 0) this += SPACE
                    this += words.pop()
                }
            }

            emitPart(messagePart, words.isEmpty())
        }
    }

    protected open fun compose(message: LogMessage): String = message.parts.combine()

    override val tagRewriteContribution: TagRewriteOperation = BuiltInTagRewriteOperation.SdkSafe

    protected open fun writeOutAndroidLog(tag: String, level: Level, cause: Throwable?, message: String) {
        if (cause == null) {
            when (level) {
                ERROR -> _androidLogger.e(tag, message)
                DEBUG -> _androidLogger.d(tag, message)
                INFO -> _androidLogger.i(tag, message)
                WARN -> _androidLogger.w(tag, message)
                VERBOSE -> _androidLogger.v(tag, message)
                OFF -> Unit
            }
        } else {
            when (level) {
                ERROR -> _androidLogger.e(tag, message, cause)
                DEBUG -> _androidLogger.d(tag, message, cause)
                INFO -> _androidLogger.i(tag, message, cause)
                WARN -> _androidLogger.w(tag, message, cause)
                VERBOSE -> _androidLogger.v(tag, message, cause)
                OFF -> Unit
            }
        }
    }

    override fun toString(): String = """
        AndroidLogAppender {
            dateCreated = "$timestamp"
            sizeLimitPerMessage = $sizeLimitPerMessage;
            continueOnNextMessage = "$continueOnNextMessage";
            appendingAtTheMoment = "${if (appending) "yes" else "no"}";
        }
    """.trimIndent()



    companion object {

        const val MAX_LOG_LINE_MESSAGE_WIDTH = 4000
        const val DEFAULT_CONTINUE_MESSAGE = " …CONTINUE ON NEXT MESSAGE… ⤦"
        private const val NONE = ""
        private const val SPACE = " "

        private fun <E> MutableList<E>.pop(): E = removeAt(0)

    }
}