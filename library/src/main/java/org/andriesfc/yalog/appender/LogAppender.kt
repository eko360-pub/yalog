package org.andriesfc.yalog.appender

import org.andriesfc.yalog.message.LogMessage

/**
 * Appends a [LogMessage] to given log.
 */
interface LogAppender {
    fun append(message: LogMessage)
}

fun logAppender(appendMessage:(LogMessage) -> Unit): LogAppender {
    return object : LogAppender {
        override fun append(message: LogMessage) {
            appendMessage(message)
        }
    }
}

fun LogAppender.toTransformingAppender(transformMessage:(LogMessage) -> LogMessage): LogAppender {
    return logAppender { transformMessage(it) }
}