package org.andriesfc.yalog.appender

import android.util.Log as android

/**
 * An enumeration of log levels.
 *
 * @param priority The priority assigned by the underlying android SDK to this level.
 *
 * @see android
 */
@Suppress("MemberVisibilityCanBePrivate")
enum class Level(val priority: Int) {

    ERROR(android.ERROR),
    DEBUG(android.DEBUG),
    INFO(android.INFO),
    WARN(android.WARN),
    VERBOSE(android.VERBOSE),
    OFF(-1)

    ;

    /**
     * Determine of the requested log level has a high enough priority to accept a log messages.
     */
    infix fun shouldAtLeastBeAllowed(requestLevel: Level): Boolean = (this != OFF) && (requestLevel.priority >= priority)
}